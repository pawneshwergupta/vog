import React from "react";
import {BrowserRouter, Route, Routes} from "react-router-dom";
import Layout from "./components/Layout";
import Home from "./pages/Home";
import University from "./pages/University";

function App() {
    return (
        <BrowserRouter>
            <Routes>
                <Route path="/" element={<Layout/>}>
                    <Route index element={<Home/>}/>
                    <Route path="/universities" element={<University/>}/>
                    {/*<Route path="/postal-lookup" element={<Home/>}/>*/}
                </Route>
            </Routes>
        </BrowserRouter>
    );
}

export default App
