import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import {fetchPostal} from "../redux/actions/postalActions";

const PostalLookup = ({ postalCodes, isLoading, error, fetchPostal }) => {
    useEffect(() => {
        fetchPostal(99950);
    }, [fetchPostal]);

    if (isLoading) {
        return <div>Loading...</div>;
    }

    if (error) {
        return <div>Error: {error}</div>;
    }

    return (
        <div>
            {postalCodes.map(postalCode => (
                <div key={postalCode.name}>
                    <h3>{postalCode.name}</h3>
                    <p>{postalCode['state-province']}</p>
                </div>
            ))}
        </div>
    );
};

const mapStateToProps = state => ({
    postalCode: state.postalCodes.postalCode,
    isLoading: state.postalCodes.isLoading,
    error: state.postalCodes.error
});

const mapDispatchToProps = (dispatch, ownProps) => ({
    find: () => dispatch(fetchPostal(ownProps.code))
});

export default connect(mapStateToProps, mapDispatchToProps)(PostalLookup);
