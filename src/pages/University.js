import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { fetchUniversities } from '../redux/actions/universityActions';

const University = ({ universities, isLoading, error, fetchUniversities }) => {
    useEffect(() => {
        fetchUniversities();
    }, [fetchUniversities]);

    if (isLoading) {
        return <div>Loading...</div>;
    }

    if (error) {
        return <div>Error: {error}</div>;
    }

    return (
        <div>
            {universities.map(university => (
                <div key={university.name}>
                    <h3>{university.name}</h3>
                    <p>{university['state-province']}</p>
                </div>
            ))}
        </div>
    );
};

const mapStateToProps = state => ({
    universities: state.universities.universities,
    isLoading: state.universities.isLoading,
    error: state.universities.error
});

const mapDispatchToProps = {
    fetchUniversities
};

export default connect(mapStateToProps, mapDispatchToProps)(University);
