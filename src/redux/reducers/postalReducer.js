const initialState = {
    postalCode: null,
    isLoading: false,
    error: null
};

const postalReducer = (state = initialState, action) => {
    switch(action.type) {
        case 'FETCH_POSTAL_REQUEST':
            return {
                ...state,
                isLoading: true
            };
        case 'FETCH_POSTAL_SUCCESS':
            return {
                ...state,
                postalCode: action.payload,
                isLoading: false,
                error: null
            };
        case 'FETCH_POSTAL_FAILURE':
            return {
                ...state,
                isLoading: false,
                error: action.payload
            };
        default:
            return state;
    }
};

export default postalReducer;
