const initialState = {
    universities: [],
    isLoading: false,
    error: null
};

const universitiesReducer = (state = initialState, action) => {
    switch(action.type) {
        case 'FETCH_UNIVERSITIES_REQUEST':
            return {
                ...state,
                isLoading: true
            };
        case 'FETCH_UNIVERSITIES_SUCCESS':
            return {
                ...state,
                universities: action.payload,
                isLoading: false,
                error: null
            };
        case 'FETCH_UNIVERSITIES_FAILURE':
            return {
                ...state,
                isLoading: false,
                error: action.payload
            };
        default:
            return state;
    }
};

export default universitiesReducer;
