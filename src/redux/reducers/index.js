// src/reducers/index.js
import { combineReducers } from 'redux';
import postsReducer from './postsReducer';
import universitiesReducer from "./universitiesReducer";
import postalReducer from "./postalReducer";

const rootReducer = combineReducers({
    posts: postsReducer,
    universities: universitiesReducer,
    postalCodes: postalReducer,
});

export default rootReducer;