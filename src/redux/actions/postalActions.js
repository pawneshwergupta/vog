import axios from 'axios';

export const fetchPostalRequest = () => ({
    type: 'FETCH_POSTAL_REQUEST'
});

export const fetchPostalSuccess = postalCode => ({
    type: 'FETCH_POSTAL_SUCCESS',
    payload: postalCode
});

export const fetchPostalFailure = error => ({
    type: 'FETCH_POSTAL_FAILURE',
    payload: error
});

export const fetchPostal = (code) => {
    return dispatch => {
        dispatch(fetchPostalRequest());
        axios.get(`https://api.zippopotam.us/us/${code}`)
            .then(response => {
                const postalCode = response.data;
                dispatch(fetchPostalSuccess(postalCode));
            })
            .catch(error => {
                const errorMsg = error.message;
                dispatch(fetchPostalFailure(errorMsg));
            });
    };
};
