import axios from 'axios';

export const fetchUniversitiesRequest = () => ({
    type: 'FETCH_UNIVERSITIES_REQUEST'
});

export const fetchUniversitiesSuccess = universities => ({
    type: 'FETCH_UNIVERSITIES_SUCCESS',
    payload: universities
});

export const fetchUniversitiesFailure = error => ({
    type: 'FETCH_UNIVERSITIES_FAILURE',
    payload: error
});

export const fetchUniversities = () => {
    return dispatch => {
        dispatch(fetchUniversitiesRequest());
        axios.get('http://universities.hipolabs.com/search?country=Canada')
            .then(response => {
                const universities = response.data;
                dispatch(fetchUniversitiesSuccess(universities));
            })
            .catch(error => {
                const errorMsg = error.message;
                dispatch(fetchUniversitiesFailure(errorMsg));
            });
    };
};
